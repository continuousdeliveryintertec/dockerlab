### Intertec Cloud Native Docker App Challenge

Once you finish the [tutorial](../beginner/chapters/votingapp.md), we encourage you to continue hacking on the app!

*All submissions are due by Monday, Oct 20th at 4pm Costa Rica Time.*

We encourage you to build a cool hack based on what you learned. Our advice is to be creative, make sure it�s useful and most importantly, have fun!

Here are some ideas the Continuous Delivery team brainstormed:

For Devs:

- Rewrite or add features to the following apps:
  * Python webapp which lets you vote between two options
  * .Net worker which consumes votes and stores them
  * Node.js webapp which shows the results of the voting in real time

For Testers:

  * Write something to generate random votes so you can load test the app

For Ops:

* Bring Docker Swarm in the mix
* Add Interlock: [github.com/ehazlett/interlock](https://github.com/ehazlett/interlock)
* Scale out the worker nodes