# Docker Tutorials and Labs

This repo contains [Docker](https://docker.com) labs and tutorials authored both by Docker, and by members of the community. We welcome contributions and want to grow the repo.

### About the Project :whale2: :boat:
- [Introduction to Docker Slides](https://docs.google.com/a/docker.com/presentation/d/1MKQ8KTxeuSYPHp7LjuOy9k8FgzAApH9i-6A1micJa1A/edit?usp=drive_web)
- Video series on setting up Docker on your machine: [Mac](https://www.youtube.com/watch?v=lNkVxDSRo7M), [Windows](https://youtu.be/S7NVloq0EBc) and [Linux](https://www.youtube.com/watch?v=V9AKvZZCWLc)

#### Docker tutorials:
* [Docker for beginners](beginner/readme.md)
* [Docker Swarm Mode](swarm-mode/README.md)
* [Configuring developer tools](developer-tools/README.md)
* [Docker for ASP.NET and Windows containers](windows/readme.md)
* [Docker for Java Developers](java/readme.adoc)
* [Dockerize a simple Node.js app](nodejs/porting/README.md)
* [Building a 12 Factor app with Docker](12factor/README.md)
* [Creating a docker registry](/registry)

#### Docker Community tutorials
* [Docker Labs](https://github.com/docker/labs)
* [Docker Tutorials from the Community](https://github.com/docker/community/blob/master/tutorials/docker-tutorials.md) - links to a different repository
* [Advanced Docker orchestration workshop](https://github.com/docker/labs/tree/master/Docker-Orchestration) - links to a different repository

For more information on Docker, see the Official [Docker documentation](https://docs.docker.com).

#### Contributing
We want to see this repo grow, so if you have a tutorial to submit please see this guide:

[Guide to submitting your own tutorial](contribute.md)