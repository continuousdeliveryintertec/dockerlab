# Private Docker Registry

This repository contains all the files necessary to create the Docker Registry

# Quick Start

To get this up and running as quickly as possible here's the steps you need to do is:

Make sure you have all the pre-reqs installed (i.e. Docker-engine 1.11):

- [CentOS 7.2](#centos-7.2)

Clone this repository and configure the Registry:

- [Registry Settings](#registry-settings)

Configure the docker hosts that you want to access this registry.

- [CentOS 7.2 Docker Hosts](#centos-7.2-docker-hosts-configuration)

If everything is configured correctly your should have a Docker Registry with an NGINX reverse proxy that uses the Azure Storage Account up and running.

## CentOS 7.2

1.  Install Docker 1.11
    1. `sudo yum -y -q update`
    2. ```sudo tee /etc/yum.repos.d/docker.repo &lt;&lt;-&#39;EOF&#39;
       [dockerrepo]
       name=Docker Repository
       baseurl=https://yum.dockerproject.org/repo/main/centos/7/
       enabled=1
       gpgcheck=1
       gpgkey=https://yum.dockerproject.org/gpg
       EOF``` Do not copy the command above from the MD format, use the raw file format instead.
       ```
    3. `sudo yum -y -q install docker-engine-1.11.2`
    4. `sudo chkconfig docker on`
    5. `sudo service docker start`
    6. `sudo usermod -aG docker <user-name>`
2.  Install Docker Compose
    1. `sudo su`
    2. ```sudo curl -L https://github.com/docker/compose/releases/download/1.8.0/docker-compose-`uname -s`-`uname -m` > /usr/bin/docker-compose```
    3. `chmod +x /usr/bin/docker-compose`
3.  Install git
    1. `sudo yum -y -q install git`

# Registry Settings
1. Clone this repository
2. Create an [Azure Storage Account](https://azure.microsoft.com/en-us/documentation/articles/storage-create-storage-account/). For other storage alternatives please check the [documentation](https://docs.docker.com/registry/configuration/#storage)
3. Create a container for the account
4. Update docker-compose file with your credentials (Storage Account -> Settings -> Access Keys)
5. Make Build
6. Make Run

# CentOS 7.2 Docker Hosts Configuration
1. `sudo systemctl edit docker` 
   1. ```[Service]
      ExecStart=
      ExecStart=/usr/bin/docker daemon --insecure-registry <ADD_REGISTRY_HOST_ADDR>``` Do not copy the configuration above from the MD format, use the raw file format instead. Please read how to set [ssl or tls configuration](https://docs.docker.com/registry/deploying/#/running-a-domain-registry) for this registry if needed.
      ```
2. Save without changing the default file name.
3. `sudo systemctl restart docker`
4. `sudo systemctl status docker`
5. Test it 
   1. `docker pull hello-world`
   2. `docker tag hello-world <ADD_REGISTRY_HOST_ADDR>/hello-there`
   3. `docker push <ADD_REGISTRY_HOST_ADDR>/hello-there`
   4. `docker pull <ADD_REGISTRY_HOST_ADDR>/hello-there`

> For more info about docker registry please check the [documentation](https://docs.docker.com/registry/)
> And the official docker registry [repo](https://github.com/docker/distribution-library-image)